# Social Network

** Functionality: **

+ registration  
+ authentication  
+ display profile  
+ edit profile  
+ ajax search with autocomplete  
+ ajax search with pagination  
+ upload avatar  
+ user export to xml  
+ user import from xml  
+ friendships  
+ creation and editing groups  
+ group memberships  
+ wall messages  
+ personal messages with webSocket chat

** Tools: **  
JDK 8, Spring 5, JPA 2 / Hibernate 5, jQuery 3, Bootstrap 4.3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 8, MySQL 5.7/H2 1.4, IntelliJIDEA 14.  


** Notes: **  
SQL ddl is located in the `db/ddl.sql`

** Screenshots **
![Login page](screenshots/loginForm.png)
![Home page](screenshots/homePage.png)
![Edit profile](screenshots/editProfile.png)
![Chat page](screenshots/chatPage.png)
![Pagination search](screenshots/paginationSearch.png)  

App's heroku link: https://socialnetwork-ibragimovv.herokuapp.com  
login: test@mail.ru  
pass: 1234  

--  
**Ibragimov Viner**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com) 