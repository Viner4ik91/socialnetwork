package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.GroupMembership;
import com.getjavajob.training.socialnetwork.common.GroupMembershipId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class GroupMembershipDaoTest extends DaoTest {

    @Autowired
    private GroupMembershipDao dao;

    private List<GroupMembership> memberships = createMemberships();
    private List<Group> groups = createGroups();
    private List<Account> accounts = createAccounts();

    @Transactional
    @Test
    public void createGroupRelationTest() {
        GroupMembership membership = new GroupMembership(groups.get(1), accounts.get(0), true, false);
        memberships.add(dao.save(membership));
        assertEquals(memberships.get(3), dao.findById(new GroupMembershipId(2, 1)).orElse(null));
    }

    @Transactional
    @Test
    public void getTest() {
        GroupMembership actual = dao.findById(new GroupMembershipId(1, 1)).orElse(null);
        assertEquals(memberships.get(1), actual);
    }

    @Transactional
    @Test
    public void getNullTest() {
        GroupMembership actual = dao.findById(new GroupMembershipId(5, 6)).orElse(null);
        assertNull(actual);
    }

    @Transactional
    @Test
    public void updateRelationTest() {
        GroupMembership membership = dao.findById(new GroupMembershipId(1, 1)).orElse(null);
        Objects.requireNonNull(membership).setAdmin(true);
        dao.save(membership);
        assertEquals(membership, dao.findById(new GroupMembershipId(1, 1)).orElse(null));
    }

    @Transactional
    @Test
    public void deleteTest() {
        dao.delete(dao.findById(new GroupMembershipId(1, 2)).orElse(null));
        assertNull(dao.findById(new GroupMembershipId(1, 2)).orElse(null));
    }

    @Transactional
    @Test
    public void getAllTest() {
        assertEquals(memberships, dao.findAll());
    }

    @Transactional
    @Test
    public void getAllAccountGroupsTest() {
        List<GroupMembership> actual = createMemberships();
        actual.remove(1);
        assertEquals(dao.getAllGroups(2), actual);
    }

    @Transactional
    @Test
    public void getAllGroupAccountsTest() {
        List<GroupMembership> actual = new ArrayList<>();
        actual.add(memberships.get(1));
        actual.add(memberships.get(0));
        assertEquals(dao.getAllAccounts(1), actual);
    }

}
