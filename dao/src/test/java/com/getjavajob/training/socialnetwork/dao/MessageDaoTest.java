package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.common.Message;
import com.getjavajob.training.socialnetwork.common.MessageType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class MessageDaoTest extends DaoTest {

    @Autowired
    private MessageDao dao;

    private List<Account> accounts = createAccounts();
    private List<Group> groups = createGroups();
    private List<Message> messages = createMessages();

    @Transactional
    @Test
    public void createWallAccountTest() {
        Message message = new Message(accounts.get(1), accounts.get(2).getId(), "4", MessageType.ACCOUNT);
        messages.add(dao.save(message));
        assertEquals(messages.get(3), dao.findById(4L).orElse(null));
    }

    @Transactional
    @Test
    public void createWallGroupTest() {
        Message message = new Message(accounts.get(1), groups.get(1).getId(), "5", MessageType.ACCOUNT);
        messages.add(dao.save(message));
        assertEquals(messages.get(3), dao.findById(4L).orElse(null));
    }

    @Transactional
    @Test
    public void createPersonalMessageTest() {
        Message message = new Message(accounts.get(1), accounts.get(2).getId(), "6", MessageType.ACCOUNT);
        messages.add(dao.save(message));
        assertEquals(messages.get(3), dao.findById(4L).orElse(null));
    }

    @Transactional
    @Test
    public void getChatTest() {
        Message personalMessage = new Message(accounts.get(1), accounts.get(0).getId(), "0", MessageType.PERSONAL);
        List<Message> expected = new ArrayList<>();
        expected.add(messages.get(2));
        expected.add(dao.save(personalMessage));
        assertEquals(expected, dao.getChat(1, 2));
    }

    @Transactional
    @Test
    public void getInterlocutorsTest() {
        Message personalMessage = new Message(accounts.get(2), accounts.get(0).getId(), "45", MessageType.PERSONAL);
        List<Long> expected = new ArrayList<>();
        expected.add(messages.get(2).getRecipientId());
        expected.add(dao.save(personalMessage).getSender().getId());
        assertEquals(expected, dao.getInterlocutors(1));
    }

    @Transactional
    @Test
    public void getAccountPostsTest() {
        Message wallMessageAccount = new Message(accounts.get(1), accounts.get(1).getId(), "-1", MessageType.ACCOUNT);
        List<Message> expected = new ArrayList<>();
        expected.add(messages.get(0));
        expected.add(dao.save(wallMessageAccount));
        assertEquals(expected, dao.findByRecipientIdAndTypeOrderByCreationDateDesc(2, MessageType.ACCOUNT));
    }

    @Transactional
    @Test
    public void getGroupPostsTest() {
        Message wallMessageGroup = new Message(accounts.get(1), groups.get(0).getId(), "-2", MessageType.GROUP);
        List<Message> expected = new ArrayList<>();
        expected.add(messages.get(1));
        expected.add(dao.save(wallMessageGroup));
        assertEquals(expected, dao.findByRecipientIdAndTypeOrderByCreationDateDesc(1, MessageType.GROUP));
    }

    @Transactional
    @Test
    public void deleteMessageTest() {
        dao.delete(dao.findById(1L).orElse(null));
        assertNull(dao.findById(1L).orElse(null));
    }

    @Transactional
    @Test
    public void getAllTest() {
        assertEquals(messages, dao.findAll());
    }

}