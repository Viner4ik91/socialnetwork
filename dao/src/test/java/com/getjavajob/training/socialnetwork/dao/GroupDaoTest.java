package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static java.sql.Date.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class GroupDaoTest extends DaoTest {

    @Autowired
    private GroupDao dao;

    private List<Group> groups = createGroups();

    @Transactional
    @Test
    public void createGroup() {
        Account creator = new Account(1, "Ibragimov", "Viner", "Ericovich", "vin@mail.ru");
        creator.setRegistrationDate(LocalDate.now());
        Group group = new Group(creator, "groupThree", "best");
        groups.add(dao.save(group));
        assertEquals(groups.get(2), dao.findById(3L).orElse(null));
    }

    @Transactional
    @Test
    public void getById() {
        Group actual = dao.findById(1L).orElse(null);
        assertEquals(groups.get(0), actual);
    }

    @Transactional
    @Test
    public void updateGroup() {
        Account creator = new Account(1, "Ibragimov", "Viner", "vin@mail.ru", "1234");
        creator.setRegistrationDate(LocalDate.now());
        Group group = new Group(1, creator, "testUpdate", "bad");
        group.setCreationDate(LocalDate.now());
        dao.save(group);
        assertEquals(group, dao.findById(1L).orElse(null));
    }

    @Transactional
    @Test
    public void deleteById() {
        dao.deleteById(2L);
        assertNull(dao.findById(2L).orElse(null));
    }

    @Transactional
    @Test
    public void getAll() {
        assertEquals(groups, dao.findAll());
    }

    @Transactional
    @Test
    public void searchEntitiesTest() {
        assertEquals(groups.get(0), dao.searchGroups("One").get(0));
    }

}