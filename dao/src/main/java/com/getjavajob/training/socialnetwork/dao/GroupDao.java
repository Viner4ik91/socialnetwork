package com.getjavajob.training.socialnetwork.dao;

import com.getjavajob.training.socialnetwork.common.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupDao extends CrudRepository<Group, Long> {

    @Query("SELECT DISTINCT g FROM Group g WHERE LOWER(g.groupName) LIKE CONCAT('%', LOWER(:value), '%')")
    List<Group> searchGroups(@Param("value") String value);

    @Query("SELECT DISTINCT g FROM Group g WHERE LOWER(g.groupName) LIKE CONCAT('%', LOWER(:value), '%')")
    List<Group> searchByPage(@Param("value") String value, Pageable pageable);

    @Query("SELECT DISTINCT COUNT(g) FROM Group g WHERE LOWER(g.groupName) LIKE CONCAT('%', LOWER(:value), '%')")
    int getGroupQty(@Param("value") String value);

}
