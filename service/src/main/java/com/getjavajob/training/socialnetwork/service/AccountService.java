package com.getjavajob.training.socialnetwork.service;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Friendship;
import com.getjavajob.training.socialnetwork.common.FriendshipId;
import com.getjavajob.training.socialnetwork.common.Message;
import com.getjavajob.training.socialnetwork.common.MessageType;
import com.getjavajob.training.socialnetwork.dao.AccountDao;
import com.getjavajob.training.socialnetwork.dao.FriendshipDao;
import com.getjavajob.training.socialnetwork.dao.MessageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private FriendshipDao friendshipDao;
    @Autowired
    private MessageDao messageDao;

    public List<Account> searchAccounts(String value) {
        return accountDao.searchAccounts(value);
    }

    public List<Account> searchByPageAccounts(String value, int page, int maxResult) {
        return accountDao.searchByPage(value, PageRequest.of(page, maxResult));
    }

    public Account createAccount(Account account) {
        account.setRegistrationDate(LocalDate.now());
        return accountDao.save(account);
    }

    public int getAccountQty(String search) {
        return accountDao.getAccountQty(search);
    }

    public Account getById(long id) {
        return accountDao.findById(id).orElse(null);
    }

    public Account getByEmail(String email) {
        return accountDao.getByEmail(email);
    }

    public void updateAccount(Account account) {
        accountDao.save(account);
    }

    @Secured("ROLE_ADMIN")
    public void deleteAccount(Account account) {
        messageDao.deleteAllBySenderOrRecipientId(account, account.getId());
        accountDao.delete(account);
    }

    //    Friendship
    public boolean checkFriendship(long accountId, long friendId) {
        Friendship friendship = friendshipDao.findById(new FriendshipId(accountId, friendId)).orElse(null);
        return friendship != null && friendship.isResponse();
    }

    public boolean checkRequestToMe(long accountId, long friendId) {
        Friendship friendship = friendshipDao.findById(new FriendshipId(accountId, friendId)).orElse(null);
        return friendship != null && friendship.getActionAccount().getId() == friendId;
    }

    public boolean checkRequestFromMe(long accountId, long friendId) {
        Friendship friendship = friendshipDao.findById(new FriendshipId(accountId, friendId)).orElse(null);
        return friendship != null && friendship.getActionAccount().getId() == accountId;
    }

    public void sendFriendRequest(Account from, Account to) {
        friendshipDao.save(new Friendship(from, to));
    }

    public void acceptFriendRequest(Account from, Account to) {
        friendshipDao.findById(new FriendshipId(from.getId(), to.getId())).ifPresent(friendship -> {
            friendship.setResponse(true);
            friendship.setActionAccount(from);
        });
    }

    public void deleteRelationship(long fromId, long toId) {
        friendshipDao.findById(new FriendshipId(fromId, toId)).ifPresent(friendship -> friendshipDao.delete(friendship));
    }

    public Map<String, List<Account>> getFriends(long id) {
        List<Account> friends = new ArrayList<>();
        List<Account> requestsFromMe = new ArrayList<>();
        List<Account> requestsToMe = new ArrayList<>();
        for (Friendship relation : friendshipDao.getAllRelations(id)) {
            if (relation.isResponse()) {
                friends.add(id == relation.getAccountFromId().getId() ? relation.getAccountToId() : relation.getAccountFromId());
            } else if (relation.getActionAccount().getId() == id) {
                requestsFromMe.add(id == relation.getAccountFromId().getId() ? relation.getAccountToId() : relation.getAccountFromId());
            } else {
                requestsToMe.add(id == relation.getAccountFromId().getId() ? relation.getAccountToId() : relation.getAccountFromId());
            }
        }
        Map<String, List<Account>> allRelations = new HashMap<>();
        allRelations.put("friends", friends);
        allRelations.put("requestsFromMe", requestsFromMe);
        allRelations.put("requestsToMe", requestsToMe);
        return allRelations;
    }

    //    Messaging
    public List<Account> getInterlocutors(long accountId) {
        List<Long> ids = messageDao.getInterlocutors(accountId);
        List<Account> interlocutors = new ArrayList<>();
        for (long id : ids) {
            interlocutors.add(accountDao.findById(id).orElse(null));
        }
        return interlocutors;
    }

    public void sendPost(Account sender, long recipientId, String text) {
        Message message = new Message(sender, recipientId, text, MessageType.ACCOUNT);
        message.setCreationDate(LocalDateTime.now());
        messageDao.save(message);
    }

    public void sendPersonalMessage(Account sender, long recipientId, String text) {
        Message message = new Message(sender, recipientId, text, MessageType.PERSONAL);
        message.setCreationDate(LocalDateTime.now());
        messageDao.save(message);
    }

    public List<Message> getAccountPosts(long accountId) {
        return messageDao.findByRecipientIdAndTypeOrderByCreationDateDesc(accountId, MessageType.ACCOUNT);
    }

    public List<Message> getChat(Account first, Account second) {
        return messageDao.getChat(first.getId(), second.getId());
    }

    public void deleteMessage(long id) {
        messageDao.deleteById(id);
    }

}
