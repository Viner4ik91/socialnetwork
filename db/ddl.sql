CREATE TABLE IF NOT EXISTS account_tbl (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    surname VARCHAR(20) NOT NULL,
    name VARCHAR(20) NOT NULL,
    middlename VARCHAR(20),
    birthday DATE,
    home_address VARCHAR(45),
    work_address VARCHAR(45),
    email VARCHAR(45) NOT NULL UNIQUE,
    icq VARCHAR(45),
    skype VARCHAR(45),
    add_info VARCHAR(45),
    password VARCHAR(65),
    admin BOOLEAN NOT NULL DEFAULT FALSE,
    image BLOB,
    registration_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS group_tbl (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    creator_id INT NOT NULL,
    group_name VARCHAR(20) NOT NULL,
    description VARCHAR(20) NOT NULL,
    image BLOB,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS phone_tbl (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    account_id INT,
    type VARCHAR(20) NOT NULL,
    number VARCHAR(20) NOT NULL UNIQUE
    CONSTRAINT account_id_for FOREIGN KEY (account_id) REFERENCES account_tbl(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS friendship_tbl (
    account_one_id INT NOT NULL,
    account_two_id INT NOT NULL,
    response BOOLEAN NOT NULL DEFAULT FALSE,
    action_account INT NOT NULL,
    CONSTRAINT friends_pr PRIMARY KEY (account_one_id, account_two_id),
    CONSTRAINT account_one_id_for FOREIGN KEY (account_one_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
    CONSTRAINT account_two_id_for FOREIGN KEY (account_two_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
    CONSTRAINT action_account_for FOREIGN KEY (action_account) REFERENCES account_tbl(id)
);

CREATE TABLE IF NOT EXISTS group_membership_tbl (
    group_id INT NOT NULL,
    account_id INT NOT NULL,
    response BOOLEAN NOT NULL DEFAULT FALSE,
    admin BOOLEAN NOT NULL DEFAULT FALSE,
    CONSTRAINT members_pr PRIMARY KEY (group_id, account_id),
    CONSTRAINT account_id_for FOREIGN KEY (account_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
    CONSTRAINT group_id_for FOREIGN KEY (group_id) REFERENCES group_tbl(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS message_tbl (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sender_id INT NOT NULL,
    recipient_id INT NOT NULL,
    msg_txt TEXT NOT NULL,
    creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    type TINYINT NOT NULL
    CONSTRAINT sender_id_for FOREIGN KEY (sender_id) REFERENCES account_tbl(id) ON DELETE CASCADE,
    CONSTRAINT recipient_id_for FOREIGN KEY (recipient_id) REFERENCES account_tbl(id) ON DELETE CASCADE
);