<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">Group members</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="moderator" items="${moderators}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${moderator.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${moderator.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/account?id=${moderator.id}">
                                    <c:out value="${moderator.name} ${moderator.surname}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${isModerator}">
                                <form action="${pageContext.request.contextPath}/deleteMember?id=${moderator.id}&groupId=${groupId}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Remove</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                <c:forEach var="member" items="${members}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${member.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${member.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/account?id=${member.id}">
                                    <c:out value="${member.name} ${member.surname}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${isModerator}">
                                <form action="${pageContext.request.contextPath}/makeModerator?id=${member.id}&groupId=${groupId}"
                                      method="POST">
                                    <button class="btn btn-primary" type="submit">
                                        <span>Up to mod</span>
                                    </button>
                                </form>
                                <form action="${pageContext.request.contextPath}/deleteMember?id=${member.id}&groupId=${groupId}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Remove</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th colspan="3">Requests</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="request" items="${requests}">
                    <tr>
                        <td style="width: 30%">
                            <c:choose>
                                <c:when test="${request.image != null}">
                                    <img src="${pageContext.request.contextPath}/printImage?id=${request.id}"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/picture/default.jpg"
                                         class="card-img-top"
                                         style="width:42px;height:56px;border:1px solid grey;"/>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td style="width: 60%; word-break: break-all;">
                            <a href="${pageContext.request.contextPath}/account?id=${request.id}">
                                    <c:out value="${request.name} ${request.surname}"/>
                        </td>
                        <td style="width: 10%">
                            <c:if test="${isModerator}">
                                <form action="${pageContext.request.contextPath}/addMember?id=${request.id}&groupId=${groupId}"
                                      method="POST">
                                    <button class="btn btn-success" type="submit">
                                        <span>Add to members</span>
                                    </button>
                                </form>
                                <form action="${pageContext.request.contextPath}/deleteMember?id=${request.id}&groupId=${groupId}"
                                      method="POST">
                                    <button class="btn btn-danger" type="submit">
                                        <span>Remove</span>
                                    </button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="common/footer.jsp" %>
