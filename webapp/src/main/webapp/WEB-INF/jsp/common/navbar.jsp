<body class="d-flex flex-column h-100">
<script src="${pageContext.request.contextPath}/resources/js/autocomplete.js"></script>
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="badge badge-primary" href="${pageContext.request.contextPath}/login">Social-network</a>
        <c:if test="${sessionScope.account != null}">
            <a class="nav-link disabled" tabindex="-1" aria-disabled="true">
                User:${sessionScope.account.surname} ${sessionScope.account.name}</a>
        </c:if>
        <c:set var="accountId"
               value="${requestScope.account.id == null ? sessionScope.account.id : requestScope.account.id}"/>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link"
                       href="${pageContext.request.contextPath}/account?id=${accountId}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="${pageContext.request.contextPath}/friends?id=${accountId}">Friends</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/chats">Messages</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="${pageContext.request.contextPath}/groups?id=${accountId}">Groups</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-outline-warning" href="${pageContext.request.contextPath}/logout">Logout</a>
                </li>
            </ul>
            <form class="form-inline mt-2 mt-md-0" style="margin-block-end:0" action="search" method="GET">
                <input id="search-ajax" name="search" class="form-control mr-sm-2" type="text" placeholder="Search"
                       aria-label="Search" required>
                <button class="btn btn-outline-light" type="submit">Search</button>
            </form>
        </div>
    </nav>
</header>