<%@include file="common/header.jsp" %>
<%@include file="common/navbar.jsp" %>

<div class="container emp-profile">
    <div class="row">
        <div class="col-md-4">
            <div class="col">
                <div class="profile-img">
                    <c:choose>
                        <c:when test="${requestScope.account.image != null}">
                            <img class='rounded-circle'
                                 src="${pageContext.request.contextPath}/printImage?id=${requestScope.account.id}">
                        </c:when>
                        <c:otherwise>
                            <img class='rounded-circle'
                                 src="${pageContext.request.contextPath}/resources/picture/default.jpg">
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="col btn">
                <c:if test="${sessionScope.account.id == requestScope.account.id}">
                    <div class="align-middle">
                        <a href="createGroup" class="btn btn-primary" type="button">Create group</a></div>
                </c:if>
                <c:if test="${requestScope.account.id != sessionScope.account.id}">
                    <c:choose>
                        <c:when test="${requestScope.isFriend}">
                            <form action="${pageContext.request.contextPath}/deleteFriend?friendId=${requestScope.account.id}&isPage=true"
                                  method="POST" class="align-middle">
                                <button class="btn btn-danger" type="submit" name="delete">Delete friend</button>
                            </form>
                        </c:when>
                        <c:when test="${isSubscribe}">
                            <form action="${pageContext.request.contextPath}/addFriend?friendId=${requestScope.account.id}"
                                  method="POST" class="align-middle">
                                <button class="btn btn-primary" type="submit" name="invite">Add friend</button>
                            </form>
                        </c:when>
                        <c:when test="${isMyRequest}">
                            <form action="${pageContext.request.contextPath}/deleteFriend?friendId=${requestScope.account.id}&isPage=true"
                                  method="POST" class="align-middle">
                                <button class="btn btn-danger" type="submit" name="delete">Remove request</button>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <form action="${pageContext.request.contextPath}/friendRequest?friendId=${requestScope.account.id}"
                                  method="POST" class="align-middle">
                                <button class="btn btn-primary" type="submit" name="invite">Send request</button>
                            </form>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </div>
            <div class="col btn">
                <c:if test="${requestScope.account.id != sessionScope.account.id}">
                    <form action="${pageContext.request.contextPath}/chats"
                          method="GET" class="align-middle">
                        <button class="btn btn-primary" type="submit" name="recipientId"
                                value="${requestScope.account.id}">Send message
                        </button>
                    </form>
                </c:if>
            </div>
        </div>
        <div class="col-md-6">
            <div class="profile-head">
                <h5>
                    ${requestScope.account.surname} ${requestScope.account.name} ${requestScope.account.middlename}
                </h5
                <h6 class="nav nav-tabs">About</h6>
            </div>
            <div class="tab-content profile-tab small">
                <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Birthday:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.birthday}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Email:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.email}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Home address:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.homeAddress}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Work address:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.workAddress}</p>
                        </div>
                    </div>
                    <h6>Phones:</h6>
                    <c:forEach var="phone" items="${requestScope.account.phones}">
                        <input type="hidden" name="">
                        <div class="row">
                            <div class="col-md-6">
                                <label>${phone.type == 'work' ? 'work' : 'person'}:</label>
                            </div>
                            <div class="col-md-6">
                                <p>${phone.number}</p>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Skype:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.skype}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>ICQ:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.icq}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Additional information:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.additionalInfo}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Registration date:</label>
                        </div>
                        <div class="col-md-6">
                            <p>${requestScope.account.registrationDate}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <c:set var="isAdminOfPage"
               value="${sessionScope.account.id == requestScope.account.id || sessionScope.account.admin}"/>
        <c:if test="${isAdminOfPage}">
            <div class="col-md-2">
                <div class="col">
                    <a href="${pageContext.request.contextPath}/updateAccount?id=${requestScope.account.id}"
                       class="btn btn-primary">Edit Profile</a>
                </div>
                <div class="col">
                    <a href="${pageContext.request.contextPath}/toXml?id=${requestScope.account.id}" class="alert">to
                        XML</a>
                </div>
                <c:if test="${sessionScope.account.admin}">
                    <div class="col">
                        <a href="deleteAccount?id=${requestScope.account.id}" class="btn btn-danger">Delete profile</a>
                    </div>
                </c:if>
            </div>
        </c:if>
    </div>
    <div class="row">
        <div class="col-md-8 offset-4">
            <div class="tab-content profile-tab">
                <hr>
                <h6>
                    What is new?
                </h6>
                <form action="${pageContext.request.contextPath}/sendPost?accountId=${requestScope.account.id}"
                      method="POST">
                    <div class="form-group">
                        <textarea class="form-control" rows="5" type="text" name="text" style="resize:none"></textarea>
                        <input class="btn btn-primary" type="submit" value="Send message" style="float: right">
                    </div>
                </form>
                <br>
                <table class="table table-hover">
                    <tbody>
                    <c:forEach var="message" items="${wallMessages}">
                        <tr>
                            <td style="width: 10%">
                                <a href="${pageContext.request.contextPath}/account?id=${message.sender.id}">
                                    <c:out value="${message.sender.name}"/></a>
                            </td>
                            <td style="width: 70%; word-break: break-all;">
                                <c:out value="${message.msgTxt}"/>
                            </td>
                            <td style="width: 30%">
                                <small>
                                    <c:out value="${message.creationDate.toLocalDate()}"/>
                                </small>
                            </td>
                            <td style="width: 1%">
                                <c:if test="${isAdminOfPage}">
                                    <small>
                                        <a href="deleteMessage?accMsgId=${message.id}&accountId=${requestScope.account.id}"
                                           class="badge badge-pill badge-danger">x</a>
                                    </small>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<%@include file="common/footer.jsp" %>
