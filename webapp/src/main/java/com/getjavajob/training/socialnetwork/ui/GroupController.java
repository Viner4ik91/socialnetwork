package com.getjavajob.training.socialnetwork.ui;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    GroupService groupService;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(byte[].class,
                new ByteArrayMultipartFileEditor());
    }

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public ModelAndView groupPage(@RequestParam("id") long id, @SessionAttribute Account account) {
        logger.info("Loading group page with id='{}'", id);
        ModelAndView modelAndView = new ModelAndView("groupView");
        Group group = groupService.getById(id);
        modelAndView.addObject("group", group);
        modelAndView.addObject("isMember", groupService.isMember(account.getId(), id));
        modelAndView.addObject("isModerator", groupService.isModerator(account.getId(), id));
        modelAndView.addObject("account", account);
        modelAndView.addObject("wallMessages", groupService.getAllGroupPosts(id));
        return modelAndView;
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.GET)
    public ModelAndView showGroupCreate() {
        ModelAndView modelAndView = new ModelAndView("groupCreation");
        modelAndView.addObject("group", new Group());
        return modelAndView;
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public String doCreation(@ModelAttribute Group group) {
        logger.info("Creating new group with name='{}'", group.getGroupName());
        Group newGroup = groupService.createGroup(group);
        return "redirect:/group?id=" + newGroup.getId();
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.GET)
    public ModelAndView showUpdate(@RequestParam("id") long id) {
        Group group = groupService.getById(id);
        ModelAndView modelAndView = new ModelAndView("groupCreation");
        modelAndView.addObject("group", group);
        return modelAndView;
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
    public String doUpdate(@ModelAttribute Group group) {
        logger.info("Updating the group with id='{}'", group.getId());
        byte[] image = groupService.getById(group.getId()).getImage();
        if (image != null) {
            group.setImage(image);
        }
        groupService.updateGroup(group);
        return "redirect:/group?id=" + group.getId();
    }

}
