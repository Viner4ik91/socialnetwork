package com.getjavajob.training.socialnetwork.ui;

import com.getjavajob.training.socialnetwork.common.Account;
import com.getjavajob.training.socialnetwork.common.Group;
import com.getjavajob.training.socialnetwork.service.AccountService;
import com.getjavajob.training.socialnetwork.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CommonController {

    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
    private static final int MAX_IMAGE_SIZE = 65000;
    private static final int MIN_IMAGE_SIZE = 0;

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/updateImage", method = RequestMethod.POST)
    public String updateImage(@RequestParam(value = "id", required = false) Long id,
                              @RequestParam(value = "groupId", required = false) Long groupId,
                              MultipartFile image,
                              HttpSession session) throws IOException {
        boolean rightImage = image.getSize() > MIN_IMAGE_SIZE && image.getSize() < MAX_IMAGE_SIZE;
        if (id != null) {
            logger.info("Update image of the account with id='{}'", id);
            Account account = accountService.getById(id);
            if (rightImage) {
                account.setImage(image.getBytes());
                accountService.updateAccount(account);
            }
            session.setAttribute("account", account);
            return "redirect:/account?id=" + id;
        } else {
            logger.info("Update image of the group with id='{}'", groupId);
            Group group = groupService.getById(groupId);
            if (rightImage) {
                group.setImage(image.getBytes());
                groupService.updateGroup(group);
            }
            return "redirect:/group?id=" + groupId;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/printImage", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] printImage(@RequestParam(value = "id", required = false) Long id,
                             @RequestParam(value = "groupId", required = false) Long groupId) {
        if (id != null) {
            Account account = accountService.getById(id);
            return account.getImage();
        } else {
            Group group = groupService.getById(groupId);
            return group.getImage();
        }
    }

    @GetMapping("/searchAjax")
    @ResponseBody
    public List<Object> showSearch(@RequestParam("search") String value) {
        logger.info("Autocomplete search of accounts and groups with pattern='{}'", value);
        List<Object> list = new ArrayList<>(accountService.searchAccounts(value));
        list.addAll(groupService.searchGroups(value));
        return list;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getSearchedList(@RequestParam(required = false, name = "search") String value,
                                  HttpServletRequest request) {
        logger.info("Show searched list with accounts and groups by pattern='{}'", value);
        request.setAttribute("value", value);
        request.setAttribute("accountQty", accountService.getAccountQty(value));
        request.setAttribute("groupQty", groupService.getGroupQty(value));
        return "searchView";
    }

    @GetMapping("/searchByPage")
    @ResponseBody
    public List<Account> searchByPage(@RequestParam("search") String value, @RequestParam int page, @RequestParam int maxResult) {
        logger.info("Pagination search of accounts with pattern='{}'", value);
        return accountService.searchByPageAccounts(value, page, maxResult);
    }

    @GetMapping("/searchByPageGroups")
    @ResponseBody
    public List<Group> searchByPageGroups(@RequestParam("search") String value, @RequestParam int page, @RequestParam int maxResult) {
        logger.info("Pagination search of groups with pattern='{}'", value);
        return groupService.searchByPageGroups(value, page, maxResult);
    }

}
