package com.getjavajob.training.socialnetwork.ui.utils;

public class JsonMessage {

    private String senderId;
    private String recipientId;
    private String msgTxt;
    private String date;


    public JsonMessage() {
    }

    public JsonMessage(String senderId, String recipientId, String msgTxt, String date) {
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.msgTxt = msgTxt;
        this.date = date;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getMsgTxt() {
        return msgTxt;
    }

    public void setMsgTxt(String msgTxt) {
        this.msgTxt = msgTxt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
