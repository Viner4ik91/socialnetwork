package com.getjavajob.training.socialnetwork.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

import static java.util.Objects.hash;

@Entity
@Table(name = "message_tbl")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "sender_id")
    private Account sender;
    @Column(name = "recipient_id")
    private long recipientId;
    @Column(name = "msg_txt")
    private String msgTxt;
    @Column(name = "creation_date", updatable = false)
    private LocalDateTime creationDate;
    @Enumerated
    private MessageType type;

    public Message() {
    }

    public Message(Account sender, long recipientId, String msgTxt, MessageType type) {
        this.sender = sender;
        this.recipientId = recipientId;
        this.msgTxt = msgTxt;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public String getMsgTxt() {
        return msgTxt;
    }

    public void setMsgTxt(String msgTxt) {
        this.msgTxt = msgTxt;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Message)) {
            return false;
        }
        Message message = (Message) o;
        return id == message.id &&
                recipientId == message.recipientId &&
                Objects.equals(sender, message.sender) &&
                Objects.equals(msgTxt, message.msgTxt) &&
                Objects.equals(creationDate.toLocalDate(), message.creationDate.toLocalDate()) &&
                type == message.type;
    }

    @Override
    public int hashCode() {
        return hash(id, sender, recipientId, msgTxt, creationDate, type);
    }

}
