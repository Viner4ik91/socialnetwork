package com.getjavajob.training.socialnetwork.common;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Objects;

@Entity
@Table(name = "phone_tbl")
@XmlAccessorType(XmlAccessType.FIELD)
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XStreamAsAttribute
    private long id;
    @Column(name = "account_id")
    @XStreamAsAttribute
    private long accountId;
    private String type;
    private String number;

    public Phone() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phone phone = (Phone) o;
        return id == phone.id &&
                accountId == phone.accountId &&
                Objects.equals(type, phone.type) &&
                Objects.equals(number, phone.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, type, number);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", type='" + type + '\'' +
                ", number='" + number + '\'' +
                '}';
    }

}
