package com.getjavajob.training.socialnetwork.common;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "group_membership_tbl")
public class GroupMembership {

    @EmbeddedId
    private GroupMembershipId id;
    @MapsId("groupId")
    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;
    @MapsId("accountId")
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account member;
    private boolean response;
    private boolean admin;

    public GroupMembership() {
    }

    public GroupMembership(Group group, Account member) {
        this(group, member, false, false);
    }

    public GroupMembership(Group group, Account member, boolean response, boolean admin) {
        this.group = group;
        this.member = member;
        this.response = response;
        this.admin = admin;
        this.id = new GroupMembershipId(group.getId(), member.getId());
    }

    public GroupMembershipId getId() {
        return id;
    }

    public void setId(GroupMembershipId id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Account getMember() {
        return member;
    }

    public void setMember(Account member) {
        this.member = member;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupMembership that = (GroupMembership) o;
        return response == that.response &&
                admin == that.admin &&
                Objects.equals(group, that.group) &&
                Objects.equals(member, that.member);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, member, response, admin);
    }

    @Override
    public String toString() {
        return "GroupMembership{" +
                "id=" + id +
                '}';
    }

}
