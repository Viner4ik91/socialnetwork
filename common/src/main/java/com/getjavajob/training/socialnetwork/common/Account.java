package com.getjavajob.training.socialnetwork.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.hash;

@Entity
@Table(name = "account_tbl")
@XStreamAlias("account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XStreamAsAttribute
    private long id;
    @Column(nullable = false)
    private String surname;
    @Column(nullable = false)
    private String name;
    private String middlename;
    @Column(nullable = false, unique = true)
    private String email;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    @Column(name = "home_address")
    private String homeAddress;
    @Column(name = "work_address")
    private String workAddress;
    private String icq;
    private String skype;
    @Column(name = "add_info")
    private String additionalInfo;
    @XStreamOmitField
    private String password;
    @Column(name = "registration_date", updatable = false)
    @XStreamOmitField
    private LocalDate registrationDate;
    @JsonIgnore
    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    @XStreamImplicit(itemFieldName = "phone")
    private List<Phone> phones = new ArrayList<>();
    @XStreamOmitField
    private boolean admin;
    @XStreamOmitField
    private byte[] image;

    public Account(String surname, String name, String email, String password) {
        this.surname = surname;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public Account(long id, String surname, String name, String email, String password) {
        this(surname, name, email, password);
        this.id = id;
    }

    public Account() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return id == account.id &&
                admin == account.admin &&
                Objects.equals(surname, account.surname) &&
                Objects.equals(name, account.name) &&
                Objects.equals(middlename, account.middlename) &&
                Objects.equals(email, account.email) &&
                Objects.equals(birthday, account.birthday) &&
                Objects.equals(homeAddress, account.homeAddress) &&
                Objects.equals(workAddress, account.workAddress) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                Objects.equals(additionalInfo, account.additionalInfo) &&
                Objects.equals(password, account.password) &&
                Objects.equals(registrationDate, account.registrationDate) &&
                Arrays.equals(image, account.image);
    }

    @Override
    public int hashCode() {
        return hash(id, surname, name, middlename, email, birthday, homeAddress, workAddress, icq, skype,
                additionalInfo, password, registrationDate, admin, image);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", registrationDate=" + registrationDate +
                '}';
    }

}
